<?php

namespace Tests\Unit;

use App\Supplier;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BasicTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
    public function testhasSupplier()
    {
        $supplier = new Supplier(['Duke Nyasinga']);
        $this->assertTrue($supplier->has('DukeNyasinga'));
        $this->assertFalse($supplier->has('item_four'));
    }
}
