<?php

namespace App\Http\Controllers\Api\V1;

use App\SupplierProduct;
use App\Http\Controllers\Controller;
use App\Http\Resources\SupplierProduct as SupplierProductResource;
use App\Http\Requests\Admin\StoreSupplierProductsRequest;
use App\Http\Requests\Admin\UpdateSupplierProductsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;



class SupplierProductsController extends Controller
{
    public function index()
    {
        

        return new SupplierProductResource(SupplierProduct::with(['supply', 'product'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('supplier_product_view')) {
            return abort(401);
        }

        $supplier_product = SupplierProduct::with(['supply', 'product'])->findOrFail($id);

        return new SupplierProductResource($supplier_product);
    }

    public function store(StoreSupplierProductsRequest $request)
    {
        if (Gate::denies('supplier_product_create')) {
            return abort(401);
        }

        $supplier_product = SupplierProduct::create($request->all());
        
        

        return (new SupplierProductResource($supplier_product))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateSupplierProductsRequest $request, $id)
    {
        if (Gate::denies('supplier_product_edit')) {
            return abort(401);
        }

        $supplier_product = SupplierProduct::findOrFail($id);
        $supplier_product->update($request->all());
        
        
        

        return (new SupplierProductResource($supplier_product))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('supplier_product_delete')) {
            return abort(401);
        }

        $supplier_product = SupplierProduct::findOrFail($id);
        $supplier_product->delete();

        return response(null, 204);
    }
}
