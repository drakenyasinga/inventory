<?php

namespace App\Http\Controllers\Api\V1;

use App\OrderDetail;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderDetail as OrderDetailResource;
use App\Http\Requests\Admin\StoreOrderDetailsRequest;
use App\Http\Requests\Admin\UpdateOrderDetailsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;



class OrderDetailsController extends Controller
{
    public function index()
    {
        

        return new OrderDetailResource(OrderDetail::with(['order', 'product'])->get());
    }

    public function show($id)
    {
        if (Gate::denies('order_detail_view')) {
            return abort(401);
        }

        $order_detail = OrderDetail::with(['order', 'product'])->findOrFail($id);

        return new OrderDetailResource($order_detail);
    }

    public function store(StoreOrderDetailsRequest $request)
    {
        if (Gate::denies('order_detail_create')) {
            return abort(401);
        }

        $order_detail = OrderDetail::create($request->all());
        
        

        return (new OrderDetailResource($order_detail))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateOrderDetailsRequest $request, $id)
    {
        if (Gate::denies('order_detail_edit')) {
            return abort(401);
        }

        $order_detail = OrderDetail::findOrFail($id);
        $order_detail->update($request->all());
        
        
        

        return (new OrderDetailResource($order_detail))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('order_detail_delete')) {
            return abort(401);
        }

        $order_detail = OrderDetail::findOrFail($id);
        $order_detail->delete();

        return response(null, 204);
    }
}
