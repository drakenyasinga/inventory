<?php
namespace App\Http\Requests\Admin;

use App\SupplierProduct;
use Illuminate\Foundation\Http\FormRequest;

class UpdateSupplierProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return SupplierProduct::updateValidation($this);
    }
}
