<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 *
 * @package App
 * @property string $name
 * @property string $description
 * @property string $quantity
*/
class Product extends Model
{
    use SoftDeletes;

    
    protected $fillable = ['name', 'description', 'quantity'];
    

    public static function storeValidation($request)
    {
        return [
            'name' => 'max:191|required',
            'description' => 'max:191|nullable',
            'quantity' => 'max:191|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'name' => 'max:191|required',
            'description' => 'max:191|nullable',
            'quantity' => 'max:191|nullable'
        ];
    }

    

    
    
    
}
