<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrderDetail
 *
 * @package App
 * @property string $order
 * @property string $product
*/
class OrderDetail extends Model
{
    use SoftDeletes;

    
    protected $fillable = ['order_id', 'product_id'];
    

    public static function storeValidation($request)
    {
        return [
            'order_id' => 'integer|exists:orders,id|max:4294967295|required',
            'product_id' => 'integer|exists:products,id|max:4294967295|required'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'order_id' => 'integer|exists:orders,id|max:4294967295|required',
            'product_id' => 'integer|exists:products,id|max:4294967295|required'
        ];
    }

    

    
    
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id')->withTrashed();
    }
    
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id')->withTrashed();
    }
    
    
}
