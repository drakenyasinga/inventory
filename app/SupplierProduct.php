<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SupplierProduct
 *
 * @package App
 * @property string $supply
 * @property string $product
*/
class SupplierProduct extends Model
{
    use SoftDeletes;

    
    protected $fillable = ['supply_id', 'product_id'];
    

    public static function storeValidation($request)
    {
        return [
            'supply_id' => 'integer|exists:suppliers,id|max:4294967295|required',
            'product_id' => 'integer|exists:products,id|max:4294967295|required'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'supply_id' => 'integer|exists:suppliers,id|max:4294967295|required',
            'product_id' => 'integer|exists:products,id|max:4294967295|required'
        ];
    }

    

    
    
    public function supply()
    {
        return $this->belongsTo(Supplier::class, 'supply_id')->withTrashed();
    }
    
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id')->withTrashed();
    }
    
    
}
