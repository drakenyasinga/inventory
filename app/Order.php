<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Order
 *
 * @package App
 * @property string $order_number
*/
class Order extends Model
{
    use SoftDeletes;

    
    protected $fillable = ['order_number'];
    

    public static function storeValidation($request)
    {
        return [
            'order_number' => 'max:191|required'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'order_number' => 'max:191|required'
        ];
    }

    

    
    
    
}
