<?php

$factory->define(App\Supplier::class, function (Faker\Generator $faker) {
    return [
        "name" => $faker->name,
    ];
});
