<?php

$factory->define(App\SupplierProduct::class, function (Faker\Generator $faker) {
    return [
        "supply_id" => factory('App\Supplier')->create(),
        "product_id" => factory('App\Product')->create(),
    ];
});
