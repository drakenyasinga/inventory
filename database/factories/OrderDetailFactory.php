<?php

$factory->define(App\OrderDetail::class, function (Faker\Generator $faker) {
    return [
        "order_id" => factory('App\Order')->create(),
        "product_id" => factory('App\Product')->create(),
    ];
});
