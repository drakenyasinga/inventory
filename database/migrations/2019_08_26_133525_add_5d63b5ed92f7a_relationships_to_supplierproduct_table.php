<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5d63b5ed92f7aRelationshipsToSupplierProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supplier_products', function(Blueprint $table) {
            if (!Schema::hasColumn('supplier_products', 'supply_id')) {
                $table->integer('supply_id')->unsigned()->nullable();
                $table->foreign('supply_id', '39696_5d63b5ecd15c5')->references('id')->on('suppliers')->onDelete('cascade');
                }
                if (!Schema::hasColumn('supplier_products', 'product_id')) {
                $table->integer('product_id')->unsigned()->nullable();
                $table->foreign('product_id', '39696_5d63b5ecd66c0')->references('id')->on('products')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supplier_products', function(Blueprint $table) {
            
        });
    }
}
