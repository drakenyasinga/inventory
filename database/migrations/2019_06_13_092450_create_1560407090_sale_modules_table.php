<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1560407090SaleModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('sale_modules')) {
            Schema::create('sale_modules', function (Blueprint $table) {
                $table->increments('id');
                $table->string('customer_first_name')->nullable();
                $table->string('customer_middle_name')->nullable();
                $table->string('customer_last_name')->nullable();
                $table->integer('quantity')->nullable();
                $table->string('item')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_modules');
    }
}
