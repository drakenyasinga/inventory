<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Drop5d035548ed5b8SaleModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('sale_modules');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(! Schema::hasTable('sale_modules')) {
            Schema::create('sale_modules', function (Blueprint $table) {
                $table->increments('id');
                $table->string('customer_first_name')->nullable();
                $table->string('customer_middle_name')->nullable();
                $table->string('customer_last_name')->nullable();
                $table->integer('quantity')->nullable();
                $table->string('item')->nullable();
                
                $table->timestamps();
                $table->softDeletes();

            $table->index(['deleted_at']);
            });
        }
    }
}
