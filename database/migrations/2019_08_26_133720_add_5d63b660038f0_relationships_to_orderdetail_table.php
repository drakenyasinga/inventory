<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5d63b660038f0RelationshipsToOrderDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_details', function(Blueprint $table) {
            if (!Schema::hasColumn('order_details', 'order_id')) {
                $table->integer('order_id')->unsigned()->nullable();
                $table->foreign('order_id', '39697_5d63b65f3edb8')->references('id')->on('orders')->onDelete('cascade');
                }
                if (!Schema::hasColumn('order_details', 'product_id')) {
                $table->integer('product_id')->unsigned()->nullable();
                $table->foreign('product_id', '39697_5d63b65f43d3e')->references('id')->on('products')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_details', function(Blueprint $table) {
            
        });
    }
}
