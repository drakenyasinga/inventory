import Vue from 'vue'
import VueRouter from 'vue-router'

import ChangePassword from '../components/ChangePassword.vue'
import PermissionsIndex from '../components/cruds/Permissions/Index.vue'
import PermissionsCreate from '../components/cruds/Permissions/Create.vue'
import PermissionsShow from '../components/cruds/Permissions/Show.vue'
import PermissionsEdit from '../components/cruds/Permissions/Edit.vue'
import RolesIndex from '../components/cruds/Roles/Index.vue'
import RolesCreate from '../components/cruds/Roles/Create.vue'
import RolesShow from '../components/cruds/Roles/Show.vue'
import RolesEdit from '../components/cruds/Roles/Edit.vue'
import UsersIndex from '../components/cruds/Users/Index.vue'
import UsersCreate from '../components/cruds/Users/Create.vue'
import UsersShow from '../components/cruds/Users/Show.vue'
import UsersEdit from '../components/cruds/Users/Edit.vue'
import OrdersIndex from '../components/cruds/Orders/Index.vue'
import OrdersCreate from '../components/cruds/Orders/Create.vue'
import OrdersShow from '../components/cruds/Orders/Show.vue'
import OrdersEdit from '../components/cruds/Orders/Edit.vue'
import SuppliersIndex from '../components/cruds/Suppliers/Index.vue'
import SuppliersCreate from '../components/cruds/Suppliers/Create.vue'
import SuppliersShow from '../components/cruds/Suppliers/Show.vue'
import SuppliersEdit from '../components/cruds/Suppliers/Edit.vue'
import ProductsIndex from '../components/cruds/Products/Index.vue'
import ProductsCreate from '../components/cruds/Products/Create.vue'
import ProductsShow from '../components/cruds/Products/Show.vue'
import ProductsEdit from '../components/cruds/Products/Edit.vue'
import SupplierProductsIndex from '../components/cruds/SupplierProducts/Index.vue'
import SupplierProductsCreate from '../components/cruds/SupplierProducts/Create.vue'
import SupplierProductsShow from '../components/cruds/SupplierProducts/Show.vue'
import SupplierProductsEdit from '../components/cruds/SupplierProducts/Edit.vue'
import OrderDetailsIndex from '../components/cruds/OrderDetails/Index.vue'
import OrderDetailsCreate from '../components/cruds/OrderDetails/Create.vue'
import OrderDetailsShow from '../components/cruds/OrderDetails/Show.vue'
import OrderDetailsEdit from '../components/cruds/OrderDetails/Edit.vue'

Vue.use(VueRouter)

const routes = [
    { path: '/change-password', component: ChangePassword, name: 'auth.change_password' },
    { path: '/permissions', component: PermissionsIndex, name: 'permissions.index' },
    { path: '/permissions/create', component: PermissionsCreate, name: 'permissions.create' },
    { path: '/permissions/:id', component: PermissionsShow, name: 'permissions.show' },
    { path: '/permissions/:id/edit', component: PermissionsEdit, name: 'permissions.edit' },
    { path: '/roles', component: RolesIndex, name: 'roles.index' },
    { path: '/roles/create', component: RolesCreate, name: 'roles.create' },
    { path: '/roles/:id', component: RolesShow, name: 'roles.show' },
    { path: '/roles/:id/edit', component: RolesEdit, name: 'roles.edit' },
    { path: '/users', component: UsersIndex, name: 'users.index' },
    { path: '/users/create', component: UsersCreate, name: 'users.create' },
    { path: '/users/:id', component: UsersShow, name: 'users.show' },
    { path: '/users/:id/edit', component: UsersEdit, name: 'users.edit' },
    { path: '/orders', component: OrdersIndex, name: 'orders.index' },
    { path: '/orders/create', component: OrdersCreate, name: 'orders.create' },
    { path: '/orders/:id', component: OrdersShow, name: 'orders.show' },
    { path: '/orders/:id/edit', component: OrdersEdit, name: 'orders.edit' },
    { path: '/suppliers', component: SuppliersIndex, name: 'suppliers.index' },
    { path: '/suppliers/create', component: SuppliersCreate, name: 'suppliers.create' },
    { path: '/suppliers/:id', component: SuppliersShow, name: 'suppliers.show' },
    { path: '/suppliers/:id/edit', component: SuppliersEdit, name: 'suppliers.edit' },
    { path: '/products', component: ProductsIndex, name: 'products.index' },
    { path: '/products/create', component: ProductsCreate, name: 'products.create' },
    { path: '/products/:id', component: ProductsShow, name: 'products.show' },
    { path: '/products/:id/edit', component: ProductsEdit, name: 'products.edit' },
    { path: '/supplier-products', component: SupplierProductsIndex, name: 'supplier_products.index' },
    { path: '/supplier-products/create', component: SupplierProductsCreate, name: 'supplier_products.create' },
    { path: '/supplier-products/:id', component: SupplierProductsShow, name: 'supplier_products.show' },
    { path: '/supplier-products/:id/edit', component: SupplierProductsEdit, name: 'supplier_products.edit' },
    { path: '/order-details', component: OrderDetailsIndex, name: 'order_details.index' },
    { path: '/order-details/create', component: OrderDetailsCreate, name: 'order_details.create' },
    { path: '/order-details/:id', component: OrderDetailsShow, name: 'order_details.show' },
    { path: '/order-details/:id/edit', component: OrderDetailsEdit, name: 'order_details.edit' },
]

export default new VueRouter({
    mode: 'history',
    base: '/admin',
    routes
})
