function initialState() {
    return {
        item: {
            id: null,
            supply: null,
            product: null,
        },
        suppliersAll: [],
        productsAll: [],
        
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    suppliersAll: state => state.suppliersAll,
    productsAll: state => state.productsAll,
    
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.supply)) {
                params.set('supply_id', '')
            } else {
                params.set('supply_id', state.item.supply.id)
            }
            if (_.isEmpty(state.item.product)) {
                params.set('product_id', '')
            } else {
                params.set('product_id', state.item.product.id)
            }

            axios.post('/api/v1/supplier-products', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.supply)) {
                params.set('supply_id', '')
            } else {
                params.set('supply_id', state.item.supply.id)
            }
            if (_.isEmpty(state.item.product)) {
                params.set('product_id', '')
            } else {
                params.set('product_id', state.item.product.id)
            }

            axios.post('/api/v1/supplier-products/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/supplier-products/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchSuppliersAll')
    dispatch('fetchProductsAll')
    },
    fetchSuppliersAll({ commit }) {
        axios.get('/api/v1/suppliers')
            .then(response => {
                commit('setSuppliersAll', response.data.data)
            })
    },
    fetchProductsAll({ commit }) {
        axios.get('/api/v1/products')
            .then(response => {
                commit('setProductsAll', response.data.data)
            })
    },
    setSupply({ commit }, value) {
        commit('setSupply', value)
    },
    setProduct({ commit }, value) {
        commit('setProduct', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setSupply(state, value) {
        state.item.supply = value
    },
    setProduct(state, value) {
        state.item.product = value
    },
    setSuppliersAll(state, value) {
        state.suppliersAll = value
    },
    setProductsAll(state, value) {
        state.productsAll = value
    },
    
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
