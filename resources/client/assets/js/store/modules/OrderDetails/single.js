function initialState() {
    return {
        item: {
            id: null,
            order: null,
            product: null,
        },
        ordersAll: [],
        productsAll: [],
        
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    ordersAll: state => state.ordersAll,
    productsAll: state => state.productsAll,
    
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.order)) {
                params.set('order_id', '')
            } else {
                params.set('order_id', state.item.order.id)
            }
            if (_.isEmpty(state.item.product)) {
                params.set('product_id', '')
            } else {
                params.set('product_id', state.item.product.id)
            }

            axios.post('/api/v1/order-details', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.order)) {
                params.set('order_id', '')
            } else {
                params.set('order_id', state.item.order.id)
            }
            if (_.isEmpty(state.item.product)) {
                params.set('product_id', '')
            } else {
                params.set('product_id', state.item.product.id)
            }

            axios.post('/api/v1/order-details/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/order-details/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchOrdersAll')
    dispatch('fetchProductsAll')
    },
    fetchOrdersAll({ commit }) {
        axios.get('/api/v1/orders')
            .then(response => {
                commit('setOrdersAll', response.data.data)
            })
    },
    fetchProductsAll({ commit }) {
        axios.get('/api/v1/products')
            .then(response => {
                commit('setProductsAll', response.data.data)
            })
    },
    setOrder({ commit }, value) {
        commit('setOrder', value)
    },
    setProduct({ commit }, value) {
        commit('setProduct', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setOrder(state, value) {
        state.item.order = value
    },
    setProduct(state, value) {
        state.item.product = value
    },
    setOrdersAll(state, value) {
        state.ordersAll = value
    },
    setProductsAll(state, value) {
        state.productsAll = value
    },
    
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
