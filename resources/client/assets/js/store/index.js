import Vue from 'vue'
import Vuex from 'vuex'
import Alert from './modules/alert'
import ChangePassword from './modules/change_password'
import Rules from './modules/rules'
import PermissionsIndex from './modules/Permissions'
import PermissionsSingle from './modules/Permissions/single'
import RolesIndex from './modules/Roles'
import RolesSingle from './modules/Roles/single'
import UsersIndex from './modules/Users'
import UsersSingle from './modules/Users/single'
import OrdersIndex from './modules/Orders'
import OrdersSingle from './modules/Orders/single'
import SuppliersIndex from './modules/Suppliers'
import SuppliersSingle from './modules/Suppliers/single'
import ProductsIndex from './modules/Products'
import ProductsSingle from './modules/Products/single'
import SupplierProductsIndex from './modules/SupplierProducts'
import SupplierProductsSingle from './modules/SupplierProducts/single'
import OrderDetailsIndex from './modules/OrderDetails'
import OrderDetailsSingle from './modules/OrderDetails/single'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        Alert,
        ChangePassword,
        Rules,
        PermissionsIndex,
        PermissionsSingle,
        RolesIndex,
        RolesSingle,
        UsersIndex,
        UsersSingle,
        OrdersIndex,
        OrdersSingle,
        SuppliersIndex,
        SuppliersSingle,
        ProductsIndex,
        ProductsSingle,
        SupplierProductsIndex,
        SupplierProductsSingle,
        OrderDetailsIndex,
        OrderDetailsSingle,
    },
    strict: debug,
})
